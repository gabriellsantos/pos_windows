﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// O modelo de item de Página em Branco está documentado em https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x416

namespace PosUniversal
{
   
    public sealed partial class MainPage : Page
    {
        string key = "8dEVcODBSbYIdLgyyPJj~353NIeuRYP3kvftTN60WGg~AogPIxN1OgN-GR-dDtCBO7S3Cw3D0tayHd9TnutTHP-g8W9Zeqr88AUmE6OoPXLk";
        public MainPage()
        {
            this.InitializeComponent();
            map.MapServiceToken = key;
            traceRouteAsync();
        }

        public async System.Threading.Tasks.Task traceRouteAsync()
        {
            //Inicio
            BasicGeoposition startLocation = new BasicGeoposition() { Latitude = -10.1769211, Longitude = -48.334366 };
            //Final.
            BasicGeoposition endLocation = new BasicGeoposition() { Latitude = -10.253497, Longitude = -48.331787  };
            //waypoints
            List<Geopoint> waypoints = new List<Geopoint>();
            waypoints.Add(new Geopoint(startLocation));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.179037, Longitude = -48.339245 }));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.181866, Longitude = -48.341500 }));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.194059, Longitude = -48.328623 }));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.194169, Longitude = -48.320249 }));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.195560, Longitude = -48.312061 }));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.215429, Longitude = -48.350989 }));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.220679, Longitude = -48.345236 }));
            waypoints.Add(new Geopoint(new BasicGeoposition() { Latitude = -10.232831, Longitude = -48.327775 }));
            waypoints.Add(new Geopoint(endLocation));

            //Obtem a rota através dos waypoints.
            MapRouteFinderResult routeResult = await MapRouteFinder.GetDrivingRouteFromWaypointsAsync(waypoints);
            if (routeResult.Status == MapRouteFinderStatus.Success)
            {
                //Cria o desenho da rota
                MapRouteView viewOfRoute = new MapRouteView(routeResult.Route);
                viewOfRoute.RouteColor = Colors.Yellow;
                viewOfRoute.OutlineColor = Colors.Black;
                //adiciona o desenho ao mapa
                map.Routes.Add(viewOfRoute);
                //Atualiza o mapa.
                await map.TrySetViewBoundsAsync(
                      routeResult.Route.BoundingBox,
                      null,
                      Windows.UI.Xaml.Controls.Maps.MapAnimationKind.None);
            }
        }
    }
}
